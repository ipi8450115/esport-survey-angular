import globals from "globals";
import tsEslint from "@typescript-eslint/eslint-plugin";
import tsParser from "@typescript-eslint/parser";
import angularEslint from "@angular-eslint/eslint-plugin";
import angularTemplateEslint from "@angular-eslint/eslint-plugin-template";
import { parseForESLint } from "@angular-eslint/template-parser";

export default [
  {
    // Cibler tous les fichiers JS, MJS, CJS et TS
    files: ["**/*.{js,mjs,cjs,ts}"],
    languageOptions: {
      globals: globals.browser, // Définir les globals pour le navigateur (par ex. window, document, etc.)
      parserOptions: {
        // Utilisez le parser TypeScript pour analyser les fichiers TS
        parser: tsParser,
        sourceType: "module", // Pour supporter les modules ECMAScript
        ecmaVersion: 2020, // Ou version supérieure si nécessaire
      },
    },
    plugins: {
      "@angular-eslint": angularEslint,
      "@typescript-eslint": tsEslint,
    },
    rules: {
      // Appliquer les règles recommandées pour TypeScript
      ...(tsEslint.configs?.recommended?.rules || {}),
      // Ajoutez vos propres règles ici
      "@typescript-eslint/no-unused-vars": ["error"],
    },
  },
  {
    // Analyser les templates Angular
    files: ["*.html"],
    languageOptions: {
      parser: parseForESLint,
    },
    plugins: {
      "@angular-eslint/template": angularTemplateEslint,
    },
    rules: {
      ...(angularTemplateEslint.configs?.recommended?.rules || {}),
    },
  },
];
