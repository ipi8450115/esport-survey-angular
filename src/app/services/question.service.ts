import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Question } from '../models/question';

@Injectable({
  providedIn: 'root',
})
export class QuestionService {
  private apiUrl = 'http://localhost:8080/api/questions';

  constructor(private http: HttpClient) {}

  getQuestionsByEvent(eventId: number): Observable<Question[]> {
    return this.http.get<Question[]>(`${this.apiUrl}?eventId=${eventId}`);
  }
}
