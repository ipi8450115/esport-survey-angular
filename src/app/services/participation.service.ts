import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ParticipationService {
  private apiUrl = 'http://localhost:8080/api/participations';

  constructor(private http: HttpClient) {}

  checkNumBilletExists(numBillet: string): Observable<boolean> {
    return this.http.get<boolean>(
      `${this.apiUrl}/exists?numBillet=${numBillet}`
    );
  }

  getClientByTicketNumber(ticketNumber: string): Observable<number> {
    return this.http.get<number>(
      `${this.apiUrl}/client-id?ticketNumber=${ticketNumber}`
    );
  }
}
