import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vote } from '../models/vote';

@Injectable({
  providedIn: 'root',
})
export class VoteService {
  private apiUrl = 'http://localhost:8080/api/votes';

  constructor(private http: HttpClient) {}

  submitVote(vote: Vote): Observable<any> {
    return this.http.post<Vote>(this.apiUrl, vote);
  }

  getVoteCounts(reponseIds: number[]): Observable<{ [key: number]: number }> {
    return this.http.get<{ [key: number]: number }>(
      `${this.apiUrl}/vote-counts?idReponses=${reponseIds.join(',')}`
    );
  }

  hasUserVoted(clientId: number, questionId: number): Observable<boolean> {
    return this.http.get<boolean>(
      `${this.apiUrl}/has-voted?clientId=${clientId}&questionId=${questionId}`
    );
  }

  getResults(questionId: number): Observable<any> {
    return this.http.get<any>(
      `${this.apiUrl}/results?questionId=${questionId}`
    );
  }
}
