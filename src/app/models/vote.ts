export interface Vote {
  idVote?: number;
  idReponse: number;
  idClient: number;
  idQuestion: number;
}
