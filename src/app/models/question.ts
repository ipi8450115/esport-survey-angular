import { Reponse } from './reponse';

export interface Question {
  idQuestion: number;
  dateFin: string;
  dateDebut: string;
  intitule: string;
  idEvent: number;
  reponses?: Reponse[];
}
