export interface Reponse {
  idReponse: number;
  libelle: string;
  idQuestion: number;
  voteCount?: number;
}
