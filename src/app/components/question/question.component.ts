import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';
import { Question } from 'src/app/models/question';
import { VoteService } from 'src/app/services/vote.service';
import { Vote } from 'src/app/models/vote';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
})
export class QuestionComponent implements OnInit {
  questions: Question[] = [];
  selectedReponses: { [key: number]: number } = {};
  hasVoted: { [key: number]: boolean } = {};

  constructor(
    private route: ActivatedRoute,
    private questionService: QuestionService,
    private voteService: VoteService,
    private router: Router
  ) {}

  ngOnInit() {
    const eventId = this.route.snapshot.params['id'];
    const clientId = Number(localStorage.getItem('clientId'));

    this.questionService.getQuestionsByEvent(eventId).subscribe(
      (data: Question[]) => {
        this.questions = data;

        const reponseIds = this.questions.flatMap((question) =>
          question.reponses
            ? question.reponses.map((reponse) => reponse.idReponse)
            : []
        );

        this.voteService.getVoteCounts(reponseIds).subscribe((voteCounts) => {
          this.questions.forEach((question) => {
            if (question.reponses) {
              question.reponses.forEach((reponse) => {
                reponse.voteCount = voteCounts[reponse.idReponse] || 0;
              });
            }
          });
        });

        this.questions.forEach((question) => {
          this.voteService
            .hasUserVoted(clientId, question.idQuestion)
            .subscribe(
              (hasVoted) => {
                this.hasVoted[question.idQuestion] = hasVoted;
              },
              (error) => {
                console.error(
                  `Erreur lors de la vérification du vote pour la question ${question.idQuestion}:`,
                  error
                );
              }
            );
        });
      },
      (error) => {
        console.error('Erreur lors de la récupération des questions:', error);
      }
    );
  }

  submitVote(questionId: number) {
    const responseId = this.selectedReponses[questionId];
    const clientId = Number(localStorage.getItem('clientId'));

    if (responseId != null && !isNaN(clientId) && clientId > 0) {
      const vote: Vote = {
        idReponse: responseId,
        idClient: clientId,
        idQuestion: questionId,
      };

      this.voteService.submitVote(vote).subscribe(
        () => {
          alert('Vote soumis avec succès!');
          this.hasVoted[questionId] = true;
        },
        (error) => {
          console.error('Erreur lors de la soumission du vote:', error);
          if (
            error.error &&
            error.error.message === 'Vous avez déjà voté pour cette question.'
          ) {
            alert('Vous avez déjà voté pour cette question.');
          } else {
            alert('Erreur lors de la soumission du vote. Veuillez réessayer.');
          }
        }
      );
    } else {
      alert('Veuillez sélectionner une réponse.');
    }
  }

  isQuestionVoted(questionId: number): boolean {
    return this.hasVoted[questionId] || false;
  }
}
