import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ParticipationService } from 'src/app/services/participation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent {
  ticketNumber: string = '';
  errorMessage: string = '';

  constructor(
    private router: Router,
    private participationService: ParticipationService
  ) {}

  onSubmit() {
    this.participationService
      .checkNumBilletExists(this.ticketNumber)
      .subscribe((exists: boolean) => {
        if (exists) {
          this.participationService
            .getClientByTicketNumber(this.ticketNumber)
            .subscribe((clientId: number) => {
              if (clientId) {
                localStorage.setItem('clientId', clientId.toString());
                console.log('clientId', clientId);
                this.router.navigate(['/events']);
              } else {
                this.errorMessage =
                  "Erreur : Impossible de récupérer l'ID client.";
              }
            });
        } else {
          this.errorMessage = 'Numéro de billet invalide. Veuillez réessayer.';
        }
      });
  }
}
