import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VoteService } from 'src/app/services/vote.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
})
export class ResultsComponent implements OnInit {
  results: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private voteService: VoteService,
    private router: Router
  ) {}

  ngOnInit() {
    const questionId = Number(this.route.snapshot.params['id']);
    this.voteService.getResults(questionId).subscribe((data: any) => {
      this.results = data;
    });
  }

  backToEvents() {
    this.router.navigate(['/event']);
  }
}
