import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from 'src/app/services/event.service';
import { Event } from 'src/app/models/event';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
})
export class EventComponent implements OnInit {
  events: Event[] = [];

  constructor(private eventService: EventService, private router: Router) {}

  ngOnInit() {
    this.eventService.getEvents().subscribe((data) => {
      //console.log('Données des événements reçues:', data);
      this.events = data;
    });
  }

  selectEvent(eventId: number) {
    if (eventId !== undefined && eventId !== null) {
      this.router.navigate(['/questions', eventId]);
    } else {
      console.error('Erreur : eventId est undefined ou null', eventId);
    }
  }
}
